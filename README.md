# 概要

標準モジュール`HttpURLConnection`をラップしたクラスライブラリ

# ビルドに必要な環境

* [Gradle](https://gradle.org/) >= 2.9
* [node.js](https://nodejs.org/en/) >= v0.12.15

# ビルド方法

1. 好きなディレクトリに`git clone https://sevenspice@bitbucket.org/sevenspice/urlconnect_wrapper.git`でプロジェクトを取得
2. ターミナルからプロジェクトに移動
3. `node server.js`でテスト用サーバーを起動
    * テスト用サーバーはポート番号8000を使用
4. 別のターミナルを起動してプロジェクに移動して`gradle build`でビルド
    * テストを行わなずビルドする場合は`gradle build -x test`
5. `build/libs/`ディレクトリ直下に`urlconnect_wrapper.jar`が生成されていることを確認