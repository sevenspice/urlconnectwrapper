package biz.libran.http;

import java.io.IOException;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
//import org.junit.Rule;
//import org.junit.rules.ExpectedException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertNull;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.net.URL;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
//import java.util.logging.Logger;

/**
 * Connection クラスの単体テスト
 */
public class ConnectionTest {
    // private static final Logger logger = Logger.getLogger(ConnectionTest.class.getName());
    //@Rule
    //public ExpectedException expectedException = ExpectedException.none();

    private String protocol;
    private String scheme;
    private String host;
    private String path;
    private int connectTimeout;
    private int readTimeout;

    @Before
    public void initialize(){

        // テストで使用するパラメーターの初期化
        this.protocol = "GET";
        this.scheme   = "http";
        this.host     = "localhost:8000";
        this.path     = "/index.html";
        this.connectTimeout = 1000;
        this.readTimeout    = 1000;

    }

    @Test
    /**
     * インスタンスの生成をテスト
     */
    public void generateConnection(){

        Map<String, String> queries  = new HashMap<>();
        Map<String, String> headers = new HashMap<>();
        queries.put("q", "tomato");
        headers.put("Content-Type", "application/x-www-form-urlencoded");

        /*
         * メンバーすべてを引数で初期化するコンストラクタのテスト
         */
        Connection connection = new Connection(
                this.protocol, this.scheme, this.host, this.path,
                queries, headers,
                this.connectTimeout, this.readTimeout
        );

        assertEquals(connection.getProtocol(), this.protocol);
        assertEquals(connection.getScheme(), this.scheme);
        assertEquals(connection.getHost(), this.host);
        assertEquals(connection.getPath(), this.path);
        assertEquals(connection.getQueries().get("q"), "tomato");
        assertEquals(connection.getHeaders().get("Content-Type"), "application/x-www-form-urlencoded");
        assertEquals(connection.getConnectTimeout(), this.connectTimeout);
        assertEquals(connection.getReadTimeout(), this.readTimeout);
        assertFalse(connection.isQueriesAdd());

        // インスタンス初期化
        connection = null;

        /*
         * タイムアウト値を指定しないコンストラクタのテスト
         */
        connection = new Connection(
                this.protocol, this.scheme, this.host, this.path,
                queries, headers
        );

        assertEquals(connection.getProtocol(), this.protocol);
        assertEquals(connection.getScheme(), this.scheme);
        assertEquals(connection.getHost(), this.host);
        assertEquals(connection.getPath(), this.path);
        assertEquals(connection.getQueries().get("q"), "tomato");
        assertEquals(connection.getHeaders().get("Content-Type"), "application/x-www-form-urlencoded");
        assertEquals(connection.getConnectTimeout(), 10000);
        assertEquals(connection.getReadTimeout(), 10000);
        assertFalse(connection.isQueriesAdd());

        // インスタンス初期化
        connection = null;

        /*
         * 接続情報のみのコンストラクタのテスト
         */
        connection = new Connection(
                this.protocol, this.scheme, this.host, this.path
        );

        assertEquals(connection.getProtocol(), this.protocol);
        assertEquals(connection.getScheme(), this.scheme);
        assertEquals(connection.getHost(), this.host);
        assertEquals(connection.getPath(), this.path);
        assertTrue(connection.getQueries().isEmpty());
        assertTrue(connection.getHeaders().isEmpty());
        assertEquals(connection.getConnectTimeout(), 10000);
        assertEquals(connection.getReadTimeout(), 10000);
        assertFalse(connection.isQueriesAdd());

    }

    // 通信処理のテストが必要な場合はコメントアウトを外すこと
    /*
    @Test
    public void executeNoArgument(){

        List<String> content;
        Map<String, String> queries  = new HashMap<>();
        Map<String, String> headers = new HashMap<>();
        queries.put("q", "tomato");
        headers.put("Content-Type", "application/x-www-form-urlencoded");

        // 実行されるURL
        String urlGet  = "http://localhost:8000/index.html?q=tomato";
        String urlPost = "http://localhost:8000/index.html";

        // 正常系 GET
        Connection connection = new Connection("GET", this.scheme, this.host, this.path);
        content = connection.execute();
        assertEquals(content.get(0), "200");
        assertEquals(content.get(1), "GET");

        // 正常系 クエリ付きGET
        connection = new Connection("GET", this.scheme, this.host, this.path);
        connection.setQueries(queries);
        content = connection.execute();
        assertEquals(content.get(0), "200");
        assertEquals(content.get(1), "q=tomato");
        assertEquals(urlGet, connection.getUrl());

        // 正常系 POST
        connection = new Connection("POST", this.scheme, this.host, this.path);
        content = connection.execute();
        assertEquals(content.get(0), "200");
        assertEquals(content.get(1), "POST");

        // 正常系 クエリ付きPOST
        connection = new Connection("POST", this.scheme, this.host, this.path);
        connection.setQueries(queries);
        content = connection.execute();
        assertEquals(content.get(0), "200");
        assertEquals(content.get(1), "q=tomato");
        assertEquals(urlPost, connection.getUrl());

        // 正常系　POSTにもURLにクエリを付加する
        connection = new Connection("POST", this.scheme, this.host, this.path);
        connection.setQueries(queries);
        connection.setQueriesAdd(true);
        content = connection.execute();
        assertEquals(content.get(0), "200");
        assertEquals(content.get(1), "q=tomato");
        assertEquals(urlGet, connection.getUrl());

        // プロトコルが null
        // 通信が実行されないこと
        connection = new Connection(null, this.scheme, this.host, this.path);
        content = connection.execute();
        // リストが空で返却されること
        assertEquals(content.size(), 0);

        // プロトコルが未対応
        // 通信が実行されないこと
        connection = new Connection("", this.scheme, this.host, this.path);
        content = connection.execute();
        // リストが空で返却されること
        assertEquals(content.size(), 0);

    }

    @Test
    public void execxute(){

        // 正常系 GET
        Connection connection = new Connection("GET", this.scheme, this.host, this.path);
        connection.execute( (Connection c) -> {
            String content = c.getContent();
            assertEquals(content, "GET");
        });

        // 正常系 POST
        connection = new Connection("POST", this.scheme, this.host, this.path);
        connection.execute( (Connection c) -> {
            String content = c.getContent();
            assertEquals(content, "POST");
        });

        // プロトコルが null
        // 通信が実行されないこと
        connection = new Connection(null, this.scheme, this.host, this.path);
        try{
            connection.execute( (Connection c) -> {
                assertTrue(false);
            });
        }catch(Exception e){
            assertTrue(false);
        }
        assertTrue(true);

        // プロトコルが未対応
        // 通信が実行されないこと
        connection = new Connection("", this.scheme, this.host, this.path);
        try{
            connection.execute( (Connection c) -> {
                assertTrue(false);
            });
        }catch(Exception e){
            assertTrue(false);
        }
        assertTrue(true);

    }

    @Test
    public void getContent()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException {

        // 通信結果を受け取るラムダ
        IHttpConnectionCallback httpConnectionCallback = new IHttpConnectionCallback(){
            public void callback(Connection connection){
                String content;
                content = connection.getContent();
                if(connection.getProtocol().equals("GET")){
                    assertEquals(content, "GET");
                }else if(connection.getProtocol().equals("POST")){
                    assertEquals(content, "POST");
                }
            }
        };

        // 通信結果を受け取るラムダ（エラー用）
        IHttpConnectionCallback httpConnectionCallbackError = new IHttpConnectionCallback(){
            public void callback(Connection connection){
                String content;
                content = connection.getContent();
                if(connection.getProtocol().equals("GET")){
                    assertEquals(content, "Not Found");
                }else if(connection.getProtocol().equals("POST")){
                    assertEquals(content, "Not Found");
                }
            }
        };


        // 正常系 GET
        Connection connection = new Connection("GET", this.scheme, this.host, this.path);
        Method getMethod = Connection.class.getDeclaredMethod("get", IHttpConnectionCallback.class);
        getMethod.setAccessible(true);
        getMethod.invoke(connection, httpConnectionCallback);

        // 正常系 POST
        connection = new Connection("POST", this.scheme, this.host, this.path);
        Method postMethod = Connection.class.getDeclaredMethod("post", IHttpConnectionCallback.class);
        postMethod.setAccessible(true);
        postMethod.invoke(connection, httpConnectionCallback);

        // エラー正常系 GET
        connection = new Connection("GET", this.scheme, this.host, "");
        Method getMethodError = Connection.class.getDeclaredMethod("get", IHttpConnectionCallback.class);
        getMethodError.setAccessible(true);
        getMethodError.invoke(connection, httpConnectionCallbackError);

        // エラー正常系 POST
        connection = new Connection("POST", this.scheme, this.host, "");
        Method postMethodError = Connection.class.getDeclaredMethod("post", IHttpConnectionCallback.class);
        postMethodError.setAccessible(true);
        postMethodError.invoke(connection, httpConnectionCallbackError);

    }

    @Test
    public void getNoArgument()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException {

        Connection connection = new Connection("GET", this.scheme, this.host, this.path);
        Method getMethod = Connection.class.getDeclaredMethod("get");
        getMethod.setAccessible(true);

        List<String> content = this.autoCast(getMethod.invoke(connection));
        assertEquals(content.get(0), "200");
        assertEquals(content.get(1), "GET");


    }

    @Test
    public void get()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException{

        // 通信結果を受け取るラムダ
        IHttpConnectionCallback httpConnectionCallback = new IHttpConnectionCallback(){
            public void callback(Connection connection){
                int statusCode = 0;
                try{
                   statusCode = connection.getConnection().getResponseCode();
                }catch(IOException e){
                    assertTrue(false);
                }
                assertEquals(statusCode, 200);
            }
        };

        // 正常系
        Connection connection = new Connection("GET", this.scheme, this.host, this.path);
        Method getMethod = Connection.class.getDeclaredMethod("get", IHttpConnectionCallback.class);
        getMethod.setAccessible(true);
        getMethod.invoke(connection, httpConnectionCallback);

        // 正常系
        // 引数にNULL
        List<String> content = this.autoCast(getMethod.invoke(connection, (Object) null));
        assertEquals(content.get(0), "200");
        assertEquals(content.get(1), "GET");

    }

    @Test
    public void postNoArgument()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException {

        Connection connection = new Connection("POST", this.scheme, this.host, this.path);
        Method postMethod =  Connection.class.getDeclaredMethod("post");
        postMethod.setAccessible(true);

        List<Map> content = this.autoCast(postMethod.invoke(connection));
        assertEquals(content.get(0), "200");
        assertEquals(content.get(1), "POST");

    }

    @Test
    public void post()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException{

        // 通信結果を受け取るラムダ
        IHttpConnectionCallback httpConnectionCallback = new IHttpConnectionCallback(){
            public void callback(Connection connection){
                int statusCode = 0;
                try{
                   statusCode = connection.getConnection().getResponseCode();
                }catch(IOException e){
                    assertTrue(false);
                }
                assertEquals(statusCode, 200);
            }
        };

        // 正常系
        Connection connection = new Connection("POST", this.scheme, this.host, this.path); 
        Method postMethod =  Connection.class.getDeclaredMethod("post", IHttpConnectionCallback.class);
        postMethod.setAccessible(true);
        postMethod.invoke(connection, httpConnectionCallback);

        // 正常系
        // 引数がNULL
        List<String> content = this.autoCast(postMethod.invoke(connection, (Object) null));
        assertEquals(content.get(0), "200");
        assertEquals(content.get(1), "POST");

    }
    */

    @Test
    public void getCharSet()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException {

        String contentType = "text/html; charset=utf-8";

        // 正常系
        Connection connection = new Connection(this.protocol, this.scheme, this.host, this.path);
        Method method =  Connection.class.getDeclaredMethod("getCharSet", String.class);
        method.setAccessible(true);
        String charset = (String) method.invoke(connection, contentType);
        assertEquals(charset, "utf-8");

        // 抽出文字パターンがない場合
        charset = (String) method.invoke(connection, "text/html; charset=");
        assertEquals(charset, "");

        // null が渡された場合
        charset = (String) method.invoke(connection, (Object) null);
        assertEquals(charset, "");

    }

    @Test
    public void generateHttpUrlConnection()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException {

        String Empty = "";

        // 正常系
        Connection connection = new Connection(this.protocol, this.scheme, this.host, this.path);
        Method method =  Connection.class.getDeclaredMethod("generateHttpUrlConnection");
        method.setAccessible(true);
        HttpURLConnection urlConnection = (HttpURLConnection) method.invoke(connection);
        // HttpURLConnection オブジェクトが返却されること
        assertThat(urlConnection, is(instanceOf(HttpURLConnection.class)));

        // URLを正しく作成できない場合はオブジェクトは返却されず null が返却されること
        connection = new Connection(this.protocol, Empty, this.host, this.path);
        urlConnection = (HttpURLConnection) method.invoke(connection);
        assertNull(urlConnection);

    }

    @Test
    public void generateURL()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException {

        String Empty = "";

        // 正常系
        Connection connection = new Connection(this.protocol, this.scheme, this.host, this.path);
        Method method =  Connection.class.getDeclaredMethod("generateURL");
        method.setAccessible(true);
        URL url = (URL) method.invoke(connection);
        // URLオブジェクトが返却されていること
        assertThat(url, is(instanceOf(URL.class)));

        // URLを正しく作成できない場合はオブジェクトは生成されず null が返却されること
        connection = new Connection(this.protocol, Empty, this.host, this.path);
        url = (URL) method.invoke(connection);
        assertNull(url);

    }

    @Test
    public void createUrl() 
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException {

        Map<String, String> querys = new HashMap<>();
        // サンプルクエリー
        querys.put("q", "tomato");
        querys.put("result", "json");

        Connection connection = new Connection(
                this.protocol, this.scheme, this.host, this.path,
                querys, new HashMap<>()
        );
        // 確認されるべき結果のURL
        String correctUrl = 
                connection.getScheme() 
                + "://" 
                + connection.getHost()
                + connection.getPath()
                + "?result=json&q=tomato";

        // リフレクションを使用してプライベートメソッドを取得
        Method method =  Connection.class.getDeclaredMethod("createUrl");
        method.setAccessible(true);
        // メソッドの実行
        String url = (String) method.invoke(connection);
        // アサーションで仕様を確認
        assertEquals(url, correctUrl);

        // プロトコルがPOSTの場合
        connection.setProtocol("POST");
        correctUrl = 
            connection.getScheme()
            + "://"
            + connection.getHost()
            + connection.getPath();
       url = (String) method.invoke(connection);
       assertEquals(url, correctUrl);

       // プロトコルがPOSTでもクエリを付加するフラグがTRUEの場合
       connection.setQueriesAdd(true);
       correctUrl = 
            connection.getScheme()
            + "://"
            + connection.getHost()
            + connection.getPath()
            + "?result=json&q=tomato";
       url = (String) method.invoke(connection);
       assertEquals(url, correctUrl);

    }

    @SuppressWarnings("unchecked")
    /**
     * キャストメソッド
     */
    private <T> T autoCast(Object object){

        T castObject = (T) object;
        return castObject;

    }

    @After
    public void disposed(){
        // 特になし
    }

}
