package biz.libran.http;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.assertEquals;

/**
 * Utility クラスの単体テスト
 */
public class UtilityTest {

    @Before
    public void initialize(){
        
    }

    @Test
    public void urlEncode(){

        // 英字・空白のエンコード (ついでに小文字への変換も確認)
        String target = "Hello World";
        String encoded = Utility.urlEncode(target);
        // テスト
        assertEquals(encoded, "Hello%20World");

        // * のエンコード
        target = "*";
        encoded = Utility.urlEncode(target);
        assertEquals(encoded, "%2a");

        // - のエンコード
        target = "-";
        encoded = Utility.urlEncode(target);
        assertEquals(encoded, "%2d");

        // _ のエンコード
        target = "_";
        encoded = Utility.urlEncode(target);
        assertEquals(encoded, "_");

        // 日本語のエンコード
        target = "艦これ";
        encoded = Utility.urlEncode(target);
        assertEquals(encoded, "%E8%89%A6%E3%81%93%E3%82%8C");

    }

    @After
    public void disposed(){
        // 特になし
    }

}
