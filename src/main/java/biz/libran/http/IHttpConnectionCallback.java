package biz.libran.http;

/**
 * HTTP通信の結果を取得するインタフェース
 */
@FunctionalInterface
public interface IHttpConnectionCallback {
    /**
     *  通信実行後のコネクションを引数に取るコールバックメソッド
     * @param connection 通信実行後の Connection クラスインスタンスが渡される
     */
    void callback(Connection connection);
}
