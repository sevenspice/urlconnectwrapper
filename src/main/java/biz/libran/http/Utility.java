package biz.libran.http;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * モジュールで使用するユーティリティ
 * @author kadowaki
 */
public class Utility {
    private static final Logger logger = Logger.getLogger(Connection.class.getName());
    
    /**
     * 引数に指定された文字列をUTF-8でURLエンコードする
     * @param target URLエンコードする文字列を指定
     * @return UTF-8でURLエンコードされた文字列を返却
     */
    public static String urlEncode(String target){
         String encode = "";

        try{

            encode = URLEncoder.encode(target, StandardCharsets.UTF_8.toString());
            // + に変換されたスペースを　%20 に置き換え　
            encode = encode.replace("+", "%20");
            // * の変換
            encode = encode.replace("*", "%2a");
            // - の変換
            encode = encode.replace("-", "%2d");

        }catch(UnsupportedEncodingException e){
            logger.log(Level.SEVERE, "method failed in Uniform Resource Locator encoding", e);
        }

        return encode;
    }
    
}
