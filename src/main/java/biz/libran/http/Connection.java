// パッケージ宣言
package biz.libran.http;

// モジュールのインポート
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * HTTP通信を実現するクラス
 * @author kadowaki
 */
public class Connection {
    // クラス内定数
    private static final int DEFAULT_CONNECT_TIMEOUT = 10000;
    private static final int DEFAULT_READ_TIMEOUT = 10000;
    // ロガー
    private static final Logger logger = Logger.getLogger(Connection.class.getName());
    // メンバー
    private String protocol;
    private String scheme;
    private String host;
    private String path;
    private String url;
    private Map<String, String> queries;
    private Map<String, String> headers;
    private int connectTimeout;
    private int readTimeout;
    private boolean queriesAdd;
    // HTTP Connection
    private HttpURLConnection connection;

    public String getProtocol() {
        return this.protocol;
    }
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getScheme() {
        return this.scheme;
    }
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getHost() {
        return this.host;
    }
    public void setHost(String host) {
        this.host = host;
    }

    public String getPath() {
        return this.path;
    }
    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl(){
        return this.url;
    }

    public int getConnectTimeout() {
        return this.connectTimeout;
    }
    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getReadTimeout() {
        return this.readTimeout;
    }
    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }
    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Map<String, String> getQueries() {
        return this.queries;
    }
    public void setQueries(Map<String, String> queries) {
        this.queries = queries;
    }

    public boolean isQueriesAdd() {
        return this.queriesAdd;
    }
    public void setQueriesAdd(boolean queriesAdd) {
        this.queriesAdd = queriesAdd;
    }

    public HttpURLConnection getConnection(){
        return this.connection;
    }

    public void setConnection(HttpURLConnection connection){
        this.connection = connection;
    }

    /**
     * コンストラクタ
     * @param protocol  GETまたはPOSTを指定可能
     * @param scheme    スキームを指定
     * @param host      通信先のホスト名を指定
     * @param path      通信先のパスを指定
     */
    public Connection(
            String protocol,
            String scheme,
            String host,
            String path
    ){
       this(protocol, scheme, host, path, new HashMap<>(), new HashMap<>());
    }

    /**
     * コンストラクタ
     * @param protocol GETまたはPOSTを指定可能
     * @param scheme  スキームを指定
     * @param host    通信先のホスト名を指定
     * @param path    通信先のパスを指定
     * @param queries HTTP通信に付加するクエリーパラメーターを指定
     * @param headers HTTP通信に付加するヘッダーパラメーターを指定
     */
    public Connection(
            String protocol,
            String scheme,
            String host,
            String path,
            Map<String, String> queries,
            Map<String, String> headers 
    ){
           this(protocol, scheme, host, path, queries, headers, DEFAULT_CONNECT_TIMEOUT, DEFAULT_READ_TIMEOUT);
    }

    /**
     * コンストラクタ
     * @param protocol  GETまたはPOSTを指定可能
     * @param scheme    スキームを指定
     * @param host      通信先のホスト名を指定
     * @param path      通信先のパスを指定
     * @param queries   HTTP通信に付加するクエリーパラメーターを指定
     * @param headers   HTTP通信に付加するヘッダーパラメーターを指定
     * @param connectTimeout 通信時のタイムアウト時間をミリ秒で指定
     * @param readTimeout    レスポンスの読み取り時のタイムアウト時間をミリ秒で指定
     */
    public Connection(
            String protocol,
            String scheme,
            String host,
            String path,
            Map<String, String> queries,
            Map<String, String> headers,
            int connectTimeout,
            int readTimeout
    ){
        this.protocol = protocol;
        this.scheme   = scheme;
        this.host     = host;
        this.path     = path;
        this.queries  = queries;
        this.headers  = headers;
        this.connectTimeout = connectTimeout;
        this.readTimeout    = readTimeout;
        this.queriesAdd = false;
    }

    /**
     * 通信を実行する
     * @return レスポンスされたコンテンツを返却
     */
    public List<String> execute(){

        String p = this.getProtocol();
        List<String> content = new ArrayList<String>();

        if(p == null){
            logger.log(Level.WARNING, "A communications protocol isn't designated. program didn't communicate.");
        }else{

            switch(p){

                case "GET":
                    content = this.get();
                    break;
                case "POST":
                    content = this.post();
                    break;
                default:
                    logger.log(Level.WARNING, "A communications protocol is incompatible. program didn't communicate.");
                    break;

            }

        }

        return content;

    }

    /**
     * 通信を実行する
     * @param ihttpConnectionCallback 通信結果の処理を行うラムダ式を指定
     */
    public void execute(IHttpConnectionCallback ihttpConnectionCallback){

        String p = this.getProtocol();

        if(p == null){
            logger.log(Level.WARNING, "A communications protocol isn't designated. program didn't communicate.");
        }else{

            switch(p){

                case "GET":
                    this.get(ihttpConnectionCallback);
                    break;
                case "POST":
                    this.post(ihttpConnectionCallback);
                    break;
                default:
                    logger.log(Level.WARNING, "A communications protocol is incompatible. program didn't communicate.");
                    break;

            }

        }

    }

    /**
     * 通信先から返却された内容を読み込む
     * @return サーバーから返却された内容
     */
    public String getContent(){

        String body = "";
        String line = "";
        String charset = "";
        String contentType = this.getConnection().getHeaderField("Content-Type");
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;


        // 文字セットの確認
        if(contentType != null){
            charset = this.getCharSet(contentType);
            if(charset.isEmpty()){
                charset = "UTF-8";
            }
        }

        try{

            if(this.getConnection() != null){

                // 読み込み準備
                inputStreamReader = new InputStreamReader(this.getConnection().getInputStream(), charset.toUpperCase());
                bufferedReader = new BufferedReader(inputStreamReader);

                // 読み込み
                while((line = bufferedReader.readLine()) != null){
                    body += line + "\r\n";
                }

                // 末尾の改行を削除
                body = body.substring(0, (body.length() - 2));

            }

        }catch(UnsupportedEncodingException e){
            logger.log(Level.SEVERE, "A problem occurred by encoding of content.", e);
        }catch(IOException e){

            try{


                if(this.getConnection() != null){

                    // 読み込み準備
                    inputStreamReader = new InputStreamReader(this.getConnection().getErrorStream(), charset);
                    bufferedReader = new BufferedReader(inputStreamReader);

                    // 読み込み
                    while((line = bufferedReader.readLine()) != null){
                        body += line + "\r\n";
                    }

                    // 末尾の改行を削除
                    body = body.substring(0, (body.length() - 2));

                }


            }catch(UnsupportedEncodingException ex){
                logger.log(Level.SEVERE, "A problem occurred by encoding of an error message.", ex);
            }catch(IOException ex){
                logger.log(Level.SEVERE, "It wasn't possible to read an error message.", ex);
            }finally{

                try{

                    if(inputStreamReader != null){
                        inputStreamReader.close();
                    }

                    if(bufferedReader != null){
                        bufferedReader.close();
                    }

                }catch(IOException exx){
                    logger.log(Level.SEVERE, "It wasn't possible to close stream.", exx);
                }


            }

        }catch(Exception e){
            logger.log(Level.SEVERE, "It wasn't possible to read content.", e);
        }finally{

            try{

                if(inputStreamReader != null){
                    inputStreamReader.close();
                }

                if(bufferedReader != null){
                    bufferedReader.close();
                }

            }catch(IOException exxx){
                logger.log(Level.SEVERE, "It wasn't possible to close stream.", exxx);
            }

        }

        return body;

    }

    /**
     * GET通信
     */
    private List<String> get(){
        return this.get(null);
    }

    /**
     * GET通信
     * @param ihttpConnectionCallback 通信結果の処理を行うラムダ式を指定
     */
    private List<String> get(IHttpConnectionCallback ihttpConnectionCallback){

        List<String> content = new ArrayList<String>();

        try{

            this.setConnection(this.generateHttpUrlConnection());
            // タイムアウト設定
            this.setTimeout();
            // リクエストヘッダー追加
            this.setRequestHeader();
            // プロトコル設定
            this.getConnection().setRequestMethod(this.getProtocol());
            this.getConnection().setDoInput(true);

            // 通信
            this.getConnection().connect();


            if(ihttpConnectionCallback != null){

                // 通信結果の処理を移譲
                ihttpConnectionCallback.callback(this);

            }else{

                content.add(String.valueOf(this.getConnection().getResponseCode()));
                content.add(this.getContent());

            }

        }catch(ProtocolException e){
            logger.log(Level.SEVERE, "A problem occurred by GET communication", e);
        }catch(IOException e){
            logger.log(Level.SEVERE, "A program failed in GET communication.", e);
        }finally{

            if(this.getConnection() != null){
                this.getConnection().disconnect();
            }

        }

        return content;

    }

    /**
     * POST通信
     */
    private List<String> post(){
        return this.post(null);
    }

    /**
     * POST通信
     * @param ihttpConnectionCallback 通信結果の処理を行うラムダ式を指定
     */
    private List<String> post(IHttpConnectionCallback ihttpConnectionCallback){

        List<String> content = new ArrayList<String>();

        try{

            this.setConnection(this.generateHttpUrlConnection());
            // タイムアウト設定
            this.setTimeout();
            // リクエストヘッダー追加
            this.setRequestHeader();
            // プロトコル設定
            this.getConnection().setRequestMethod(this.getProtocol());
            this.getConnection().setDoOutput(true);
            // BODYにクエリを追加
            this.setQueriesInBody();

            // 通信
            this.getConnection().connect();

            if(ihttpConnectionCallback != null){

                // 通信結果の処理を移譲
                ihttpConnectionCallback.callback(this);

            }else{

                content.add(String.valueOf(this.getConnection().getResponseCode()));
                content.add(this.getContent());

            }

        }catch(ProtocolException e){
            logger.log(Level.SEVERE, "A problem occurred by POST communication", e);
        }catch(IOException e){
            logger.log(Level.SEVERE, "A program failed in POST communication.", e);
        }finally{

            if(this.getConnection() != null){
                this.getConnection().disconnect();
            }

        }

        return content;

    }

    /**
     * Content-Typeから文字セットを抽出する
     * @param contentType Content-Typeの文字列を指定
     * @return 抽出された文字セット
     */
    private String getCharSet(String contentType){

        String charset = "";

        // 文字セットの抽出
        if(contentType != null){

            Pattern pattern = Pattern.compile("(.*); charset=(.*)");
            Matcher matcher = pattern.matcher(contentType);
            if(matcher.find()){
                charset = matcher.group(2);
            }

        }

        return charset;

    }

    /**
     * クエリ情報をPOST通信の場合, BODY要素に付加する
     */
    private void setQueriesInBody() throws IOException{

        String queryString = "";

        if(this.getConnection() != null){

            if(this.getQueries() != null && !this.getQueries().isEmpty()){

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(this.getConnection().getOutputStream(), StandardCharsets.UTF_8));

                // クエリ文字列を生成
                for(Map.Entry<String, String> map : this.getQueries().entrySet()){
                    queryString += map.getKey() + "=" + Utility.urlEncode(map.getValue()) + "&";
                }
                queryString = queryString.substring(0, (queryString.length() - 1));

                // 書き込み
                writer.write("\r\n");
                writer.write(queryString);
                writer.write("\r\n");
                writer.flush();

            }

        }

    }

    /**
     * コネクションにリクエストヘッダーをセットする
     */
    private void setRequestHeader(){

        if(this.getHeaders() != null && this.getConnection() != null){
            for(Map.Entry<String, String> map : this.getHeaders().entrySet()){
                this.getConnection().setRequestProperty(map.getKey(), map.getValue());
            }
        }

    }

    /**
     * コネクションにタイムアウト値をセットする
     */
    private void setTimeout(){

        if(this.getConnection() != null){
            this.getConnection().setConnectTimeout(this.getConnectTimeout());
            this.getConnection().setReadTimeout(this.getReadTimeout());
        }

    }

    /**
     * HTTPコネクションを返却する
     * @return 開いたHTTPコネクションを返却
     */
    private HttpURLConnection generateHttpUrlConnection(){

        URL url = this.generateURL();
        if(url != null){

            try{
                return (HttpURLConnection) url.openConnection();
            }catch(IOException e){
                logger.log(Level.SEVERE, "It wasn't possible to hold a http connection", e);
            }

        }

        return null;

    }

    /**
     * URLオブジェクトを生成して返却する
     * @return 生成されたURLオブジェクトを返却
     */
    private URL generateURL(){

        try{
            return new URL(this.createUrl());
        }catch(MalformedURLException e){
            logger.log(Level.SEVERE, "Uniform Resource Locator object couldn't be generated", e);
        }

        return null;

    }

    /**
     * インスタンスに設定されたメンバーから接続先のURLを作成して返却する
     * @return 作成されたURL
     */
    private String createUrl(){
        String baseUrl = this.getScheme() + "://" + this.getHost() + this.getPath();
        if(!this.getQueries().isEmpty()){
            baseUrl += '?';
        }

        if(this.getProtocol() != null){

            switch(this.getProtocol()){

                case "GET":
                    for(Map.Entry<String, String> query : this.getQueries().entrySet()){
                        baseUrl += query.getKey() + "=" + Utility.urlEncode(query.getValue()) + "&";
                    }
                    break;
                case "POST":
                    if(this.isQueriesAdd()){
                        for(Map.Entry<String, String> query : this.getQueries().entrySet()){
                            baseUrl += query.getKey() + "=" + Utility.urlEncode(query.getValue()) + "&";
                        }
                    }
                    break;
                default:
                    logger.warning("A protocol corresponds to only GET and POST.");
                    break;

            }

        }else{
            logger.severe("A protocol is unclear.");
        }

        // 末尾の&を削除する
        if(!this.getQueries().isEmpty()){
            baseUrl = baseUrl.substring(0, (baseUrl.length() - 1));
        }

        // インスタンスに必要な時に参照できるようにセットしておく
        this.url = baseUrl;
        // 返却
        return baseUrl;
    }
}
