/**
 * HTTPクライアント用簡易テストサーバー
 */

/*global require */
var http = require('http');
var fs   = require('fs');
var url  = require('url');

/*global console */
// サーバー生成とサーバー起動
var server = http.createServer(function (request, response) {
    'use strict';
    console.log(request.method);

    if (request.method === 'GET') {
        console.log(request.headers);

        if (request.url === '/index.html') {
            response.statusCode = 200;
            response.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
            response.end('GET');
        } else {

            var parse = url.parse(request.url);
            if(parse.query != null){

                response.statusCode = 200;
                response.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
                response.end(parse.query);


            }else{

                response.statusCode = 404;
                response.writeHead(404, {'Content-Type': 'text/plain; charset=utf-8'});
                response.end('Not Found');

            }
        }

    } else if (request.method === 'POST') {
        console.log(request.headers);

        var postQuery = "";

        // リクエストボディの読み込み
        request.on('readable', function (chunk) {
            var line = request.read();
            if (line !== null) {
                postQuery += line;
            }
        });

        //リクエストボディをすべて読み込んだらendイベントが発火する。
        request.on('end', function () {

            if (request.url === '/index.html') {

                if(postQuery != ""){

                    response.statusCode = 200;
                    response.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
                    response.end(postQuery.replace(/\r?\n/g,"").replace(" ", ""));

                }else{

                    response.statusCode = 200;
                    response.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
                    response.end('POST');

                }

            } else {

                if(postQuery != ""){

                    response.statusCode = 200;
                    response.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
                    response.end(postQuery.replace(/\r?\n/g,"").replace(" ", ""));

                }else{

                    response.statusCode = 404;
                    response.writeHead(404, {'Content-Type': 'text/plain; charset=utf-8'});
                    response.end('Not Found');

                }

            }

        });

    }

}).listen(8000);
